module SuaveMusicStore.App

open System
open System.Net

open Suave                 // always open suave
open Suave.Authentication
open Suave.Cookie
open Suave.Filters
open Suave.Form
open Suave.Model.Binding
open Suave.Operators
open Suave.RequestErrors
open Suave.State.CookieStateStore
open Suave.Successful      // for OK-result
open Suave.Web             // for config

type UserLoggedOnSession = {
    Username : string
    Role : string
}

type Session  = 
    | NoSession
    | CartIdOnly of string
    | UserLoggedOn of UserLoggedOnSession    

let session f =
    statefulForSession
    >=> context (fun x ->
        match x |> HttpContext.state with
        | None -> f NoSession
        | Some state ->
            match state.get "username", state.get "role" with
            | Some username, Some role ->
                f (UserLoggedOn {Username = username; Role = role})
            | _ -> f NoSession)

let html container =
    let result user =
        OK (View.index (View.partUser user) container)
        >=> Writers.setMimeType "text/html; charset=utf-8"
    session (function
    | UserLoggedOn { Username = username } -> result (Some username)
    | _ -> result None)    

let browse =
    request (fun r ->
        match r.queryParam Path.Store.browseKey with
        | Choice1Of2 genre -> 
            Db.getContext()
            |> Db.getAlbumsForGenre genre
            |> View.browse genre
            |> html
        | Choice2Of2 msg -> BAD_REQUEST msg)

let overview = warbler (fun _ ->
    Db.getContext()
    |> Db.getGenres
    |> List.map (fun g -> g.Name)
    |> View.store
    |> html)

let details id = 
    match Db.getAlbumDetails id (Db.getContext()) with
    | Some album -> 
        html (View.details album)
    | None ->
        never

let manage = warbler (fun _ ->
    Db.getContext()
    |> Db.getAlbumsDetails
    |> View.manage
    |> html)

let bindToForm form handler =
    bindReq (bindForm form) handler BAD_REQUEST
let createAlbum = 
    let ctx = Db.getContext()
    choose [
        GET >=> warbler (fun _ ->
            let genres =
                Db.getGenres ctx
                |> List.map (fun g -> decimal g.Genreid, g.Name)
            let artists =
                Db.getArtist ctx
                |> List.map (fun g -> decimal g.Artistid, g.Name)            
            html (View.createAlbum genres artists ))            
    ]

let deleteAlbum id = 
    let ctx = Db.getContext()
    match Db.getAlbum id ctx with
    | Some album ->
        choose [
            GET >=> warbler (fun _ ->
                html (View.deleteAlbum album.Title))
            POST >=> warbler (fun _ ->
                Db.deleteAlbum album ctx;
                Redirection.FOUND Path.Admin.manage)            
        ]
    | None ->
        never

let editAlbum id =
    let ctx = Db.getContext()
    match Db.getAlbum id ctx with
    | Some album ->
        choose [
            GET >=> warbler (fun _ ->
                let genres =
                    Db.getGenres ctx
                    |> List.map (fun g -> decimal g.Genreid, g.Name)
                let artists = 
                    Db.getArtist ctx
                    |> List.map (fun a -> decimal a.Artistid, a.Name)
                html (View.editAlbum album genres artists))
            POST >=> bindToForm Form.album (fun form ->
                Db.updateAlbum
                    album
                    (int form.ArtistId,
                     int form.GenreId,
                     form.Price,
                     form.Title) ctx
                Redirection.FOUND Path.Admin.manage)
        ]
    | None -> never    




let passHash (pass : string) =
    use sha = Security.Cryptography.SHA256.Create()
    Text.Encoding.UTF8.GetBytes(pass)
    |> sha.ComputeHash
    |> Array.map (fun b -> b.ToString("x2"))
    |> String.concat ""

let sessionStore setF = context (fun x ->
    match HttpContext.state x with
    | Some state -> setF state
    | None -> never)

let returnPathOrHome =
    request (fun x -> 
        let path =
            match (x.queryParam "returnPath") with
            | Choice1Of2 path -> path
            | _ -> Path.home
        Redirection.FOUND path)
let logon =
    choose [
        GET >=> (View.logon "" |> html)
        POST >=> bindToForm Form.logon (fun form ->
            let ctx = Db.getContext()
            let (Password password) = form.Password
            match Db.validateUser(form.Username, passHash password) ctx with
            | Some user ->
                    authenticated Cookie.CookieLife.Session false 
                    >=> session (fun _ -> succeed)
                    >=> sessionStore (fun store ->
                        store.set "username" user.Username
                        >=> store.set "role" user.Role)
                    >=> returnPathOrHome
            | _ ->
                View.logon "Username or password is invalid." |> html
        )
    ]

let reset =
    unsetPair SessionAuthCookie 
    >=> unsetPair StateCookie
    >=> Redirection.FOUND Path.home

let redirectWithReturnPath redirection =
    request (fun x ->
        let path = x.url.AbsolutePath
        Redirection.FOUND (redirection |> Path.withParam ("returnPath", path)))

let loggedOn fsuccess =
    authenticate
        Cookie.CookieLife.Session
        false
        (fun () -> Choice2Of2(redirectWithReturnPath Path.Account.logon))
        (fun _ -> Choice2Of2 reset)
        fsuccess

let admin fsuccess =
    loggedOn (session (function
        | UserLoggedOn { Role = "admin" } -> fsuccess
        | UserLoggedOn _ -> FORBIDDEN "Only for admin"
        | _ -> UNAUTHORIZED "Not logged in"
    ))        
let cart = View.cart [] |> html
let addToCart albumId =
    let ctx = Db.getContext()
    session (function
        | NoSession ->
            let cartId = Guid.NewGuid().ToString("N")
            Db.addToCart cartId albumId ctx
            sessionStore (fun store ->
                store.set "cartid" cartId)
        | UserLoggedOn {Username = cartId } | CartIdOnly cartId -> 
            Db.addToCart cartId albumId ctx
            succeed) >=> Redirection.FOUND Path.Cart.overview 
let webPart = 
    choose [
        path Path.home >=> html View.home 
        path Path.Store.overview >=> overview
        path Path.Store.browse >=> browse
        pathScan Path.Store.details details

        path Path.Admin.manage >=> admin manage
        path Path.Admin.createAlbum >=> admin createAlbum
        pathScan Path.Admin.editAlbum (fun id -> admin (editAlbum id))
        pathScan Path.Admin.deleteAlbum (fun id -> admin (deleteAlbum id))
        
        path Path.Account.logon  >=> logon
        path Path.Account.logoff >=> reset
        path Path.Cart.overview >=> cart
        pathScan Path.Cart.addAlbum addToCart

        pathRegex "(.*)\.(css|png|gif)" >=> Files.browseHome
        html View.notFound
    ]



let cfg = 
    { defaultConfig with
        bindings =
            [ HttpBinding.create HTTP IPAddress.Loopback 8084us ]
        listenTimeout = TimeSpan.FromMilliseconds 3000. }

startWebServer cfg webPart
